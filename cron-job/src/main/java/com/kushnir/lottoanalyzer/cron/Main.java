package com.kushnir.lottoanalyzer.cron;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class Main {
    public static void main(String[] args) {
        final JobDetail CRON_JOB = JobBuilder.newJob(CronJob.class)
                .withIdentity("cronJob", "group1").build();

        // .withSchedule(CronScheduleBuilder.cronSchedule("0 * 0 ? * * *"))
        final Trigger TRIGGER = TriggerBuilder.newTrigger()
                .withIdentity("cronJobTrigger1", "group1")
                .withSchedule(CronScheduleBuilder.atHourAndMinuteOnGivenDaysOfWeek(8, 0, 3, 4, 6, 1))
                // .withSchedule(CronScheduleBuilder.cronSchedule("0 * 0 ? * * *"))
                .build();
        try {
            final Scheduler SCHEDULER = new StdSchedulerFactory().getScheduler();
            SCHEDULER.start();
            SCHEDULER.scheduleJob(CRON_JOB, TRIGGER);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
