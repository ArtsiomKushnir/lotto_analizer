package com.kushnir.lottoanalyzer.cron;

import com.kushnir.lottoanalyzer.service.ByService;
import com.kushnir.lottoanalyzer.service.PlEuroService;
import com.kushnir.lottoanalyzer.service.PlService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.io.StringWriter;

import static com.kushnir.lottoanalyzer.service.mail.MailSender.sendEmail;

public class CronJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            StringWriter printWriter = new StringWriter();

            new PlService(printWriter).handle();
            new PlEuroService(printWriter).handle();
            new ByService(printWriter).handle();

            String theString = printWriter.toString();

            sendEmail(theString);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
