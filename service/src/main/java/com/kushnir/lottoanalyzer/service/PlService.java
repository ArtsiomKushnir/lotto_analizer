package com.kushnir.lottoanalyzer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kushnir.lottoanalyzer.service.dto.PlResults;
import lombok.RequiredArgsConstructor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.String.format;

@RequiredArgsConstructor
public class PlService {

    private final String URL_LOTTO_RESULTS_JSON = "https://www.lotto.pl/api/lotteries/draw-results/by-gametype?game=Lotto&index=1&size=7000&sort=drawDate&order=DESC";
    private final static String PL_LOTTO_RESULTS_FILE = "/home/akushnir/PROJECTS/lotto_analizer/console-app/src/resources/PL_6x49_04-09-2022_results.json";
    private final static Integer[][] SELECTED_NUMBERS = new Integer[][] {{2, 16, 34, 37, 41, 45}, {5, 14, 17, 30, 31, 47}};

    private final int countToMatch;

    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Writer printWriter;

    public void handle() throws IOException {
        printWriter.append("========  PL  ========\n").append("Selected Combination: ").append(Arrays.deepToString(SELECTED_NUMBERS)).append("\n");

        String s = new String(new BufferedInputStream(new URL(URL_LOTTO_RESULTS_JSON).openStream()).readAllBytes(), StandardCharsets.UTF_8);

        final PlResults plResults = OBJECT_MAPPER.readValue(s, PlResults.class);
        AtomicBoolean matchFound = new AtomicBoolean(false);

        plResults.getItems().forEach(lottoItem -> {
            lottoItem.getResults().stream().filter(lottoResult -> "Lotto".equals(lottoResult.getGameType())).forEach(lottoResult -> {
                ArrayList<Integer> winNumbers = lottoResult.getResultsJson();

                int countMatch = 0;

                for (Integer[] givenNumber : SELECTED_NUMBERS) {
                    for (Integer n : givenNumber) {
                        if (winNumbers.contains(n)) {
                            countMatch++;
                        }
                    }

                    if(countMatch > countToMatch) {
                        matchFound.set(true);
                        try {
                            printWriter.append(format("Match -> %s,  %s%n", winNumbers, lottoResult.getDrawDate()));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    countMatch = 0;
                }
            });
        });

        if (!matchFound.get()) {
            try {
                printWriter.append("No matches found for: ").append(Arrays.deepToString(SELECTED_NUMBERS)).append("\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
