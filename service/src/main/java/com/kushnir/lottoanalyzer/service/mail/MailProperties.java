package com.kushnir.lottoanalyzer.service.mail;

class MailProperties {
    static final String MAIL_SERVER_USERNAME = System.getProperty("mail.username");
    static final String MAIL_SERVER_PASSWORD = System.getProperty("mail.pass");
    static final boolean MAIL_SMTP_AUTH =
            Boolean.parseBoolean(System.getProperty("mail.smtp.auth" , "true"));
    static final boolean MAIL_SMTP_STARTTLS_ENABLED =
            Boolean.parseBoolean(System.getProperty("mail.smtp.starttls.enable" , "true"));
    static final String MAIL_SMTP_HOST = System.getProperty("mail.smtp.host" , "smtp.mail.ru");
    static final String MAIL_SMTP_PORT = System.getProperty("mail.smtp.port" , "465");
    static final String MAIL_SMTP_SSL_TRUST = System.getProperty("mail.smtp.ssl.trust" , "smtp.mail.ru");

    static final String MAIL_FROM = System.getProperty("mail.username", "no_reply@lotto_analizer.com");
}
