package com.kushnir.lottoanalyzer.service.util;

import java.io.IOException;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

public class Util {
    public static String readJsonAsString(final String fileName) throws IOException {
        return new String(readAllBytes(get(fileName)));
    }
}
