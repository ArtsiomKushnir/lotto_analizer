package com.kushnir.lottoanalyzer.service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class ByService {

    private final static String URL_6x49_RESULTS_FILE = "https://6x49.sportloto.by/media/results.xlsx";
    private final static String URL_5x36_RESULTS_FILE = "https://sportloto.by/media/results.xlsx";

    private final static String[] selectedCombinations = {"31118212430", "193031374047"};
    private final static Integer[][] selected6x49CombinationsInt = {
            new Integer[]{3, 11, 18, 21, 24, 30},
            new Integer[]{19, 30, 31, 37, 40, 47}
    };

    private final static Integer[][] selected5x36CombinationsInt = {
            new Integer[]{6, 13, 17, 25, 30}
    };

    private final Writer printWriter;

    public ByService(Writer printWriter) {
        this.printWriter = printWriter;
    }

    public void handle() throws IOException {
        try {
            configure();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

        printWriter.append("========  BY  ========\n");

        check6x49();
        check5x36();

        printWriter.flush();
    }

    public void configure() throws NoSuchAlgorithmException, KeyManagementException {
        final TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {  }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {  }

                }
        };

        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        final HostnameVerifier allHostsValid = (hostname, session) -> true;

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    private void check5x36() throws IOException {
        printWriter.append("5 x 36:");

        BufferedInputStream bis = new BufferedInputStream(new URL(URL_5x36_RESULTS_FILE).openStream());

        HashSet<Integer[]> setOfCombinations = new HashSet<>();
        HashMap<String, Integer[]> combinations = new HashMap<>();
        HashMap<Integer, Integer> numberCounts = new HashMap<>();

        XSSFWorkbook workbook = new XSSFWorkbook(bis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIt = sheet.iterator();
        rowIt.next();

        while (rowIt.hasNext()) {
            Row row = rowIt.next();

            Cell cell2 = row.getCell(2);
            Cell cell3 = row.getCell(3);
            Cell cell4 = row.getCell(4);
            Cell cell5 = row.getCell(5);
            Cell cell6 = row.getCell(6);

            Integer n1 = (int) cell2.getNumericCellValue();
            Integer n2 = (int) cell3.getNumericCellValue();
            Integer n3 = (int) cell4.getNumericCellValue();
            Integer n4 = (int) cell5.getNumericCellValue();
            Integer n5 = (int) cell6.getNumericCellValue();

            Integer[] numbers = {n1, n2, n3, n4, n5};
            Arrays.sort(numbers);

            processNumberCount(numberCounts, numbers);

            setOfCombinations.add(numbers);
            process5Combinations(combinations, numbers);
        }

        checkNum5Combinations(setOfCombinations);

        workbook.close();
        bis.close();
    }

    private void check6x49() throws IOException {
        printWriter.append("6 x 49:");

        BufferedInputStream bis = new BufferedInputStream(new URL(URL_6x49_RESULTS_FILE).openConnection().getInputStream());

        HashSet<Integer[]> setOfCombinations = new HashSet<>();
        HashMap<String, Integer[]> combinations = new HashMap<>();
        HashMap<Integer, Integer> numberCounts = new HashMap<>();

        XSSFWorkbook workbook = new XSSFWorkbook(bis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIt = sheet.iterator();
        rowIt.next();

        while (rowIt.hasNext()) {
            Row row = rowIt.next();

            Cell cell2 = row.getCell(2);
            Cell cell3 = row.getCell(3);
            Cell cell4 = row.getCell(4);
            Cell cell5 = row.getCell(5);
            Cell cell6 = row.getCell(6);
            Cell cell7 = row.getCell(7);

            Integer n1 = (int) cell2.getNumericCellValue();
            Integer n2 = (int) cell3.getNumericCellValue();
            Integer n3 = (int) cell4.getNumericCellValue();
            Integer n4 = (int) cell5.getNumericCellValue();
            Integer n5 = (int) cell6.getNumericCellValue();
            Integer n6 = (int) cell7.getNumericCellValue();

            Integer[] numbers = {n1, n2, n3, n4, n5, n6};
            Arrays.sort(numbers);

            processNumberCount(numberCounts, numbers);

            setOfCombinations.add(numbers);
            process6Combinations(combinations, numbers);
        }

        // getRandomCombination(combinations);

        checkNum6Combinations(setOfCombinations);

        workbook.close();
        bis.close();
    }

    private void checkNum6Combinations(final HashSet<Integer[]> setOfCombinations) throws IOException {
        boolean notFound = true;
        for (Integer[] combinations : setOfCombinations) {
            for (Integer[] givenCombinations : selected6x49CombinationsInt) {

                int countMatches = 0;

                for (Integer givenNumber : givenCombinations) {
                    for (Integer number : combinations) {
                        if (givenNumber.equals(number)) {
                            countMatches++;
                        }
                    }
                }

                if (countMatches > 3) {
                    notFound = false;

                    String givenCombinationsString = String.format("%s-%s-%s-%s-%s-%s", givenCombinations[0], givenCombinations[1], givenCombinations[2], givenCombinations[3], givenCombinations[4], givenCombinations[5]);
                    String combinationsString = String.format("%s-%s-%s-%s-%s-%s", combinations[0], combinations[1], combinations[2], combinations[3], combinations[4], combinations[5]);

                    printWriter.append(String.format("%s > 3 matches in: %s", givenCombinationsString, combinationsString));
                }
            }
        }

        if (notFound) {
            printWriter.append("No matches found for: ").append(Arrays.deepToString(selected6x49CombinationsInt)).append("\n");
        }
    }

    private void checkNum5Combinations(final HashSet<Integer[]> setOfCombinations) throws IOException {
        boolean notFound = true;
        for (Integer[] combinations : setOfCombinations) {
            for (Integer[] givenCombinations : selected5x36CombinationsInt) {

                int countMatches = 0;

                for (Integer givenNumber : givenCombinations) {
                    for (Integer number : combinations) {
                        if (givenNumber.equals(number)) {
                            countMatches++;
                        }
                    }
                }

                if (countMatches > 3) {
                    notFound = false;
                    String givenCombinationsString = String.format("%s-%s-%s-%s-%s", givenCombinations[0], givenCombinations[1], givenCombinations[2], givenCombinations[3], givenCombinations[4]);
                    String ombinationsString = String.format("%s-%s-%s-%s-%s", combinations[0], combinations[1], combinations[2], combinations[3], combinations[4]);

                    printWriter.append(String.format("%s > 3 matches in: %s", givenCombinationsString, ombinationsString));
                }
            }
        }

        if (notFound) {
            printWriter.append("No matches found for: ").append(Arrays.deepToString(selected5x36CombinationsInt));
        }
    }

    private void processNumberCount(final HashMap<Integer, Integer> numberCounts, final Integer[] numbers) {
        for (Integer number : numbers) {
            Integer n = numberCounts.get(number);
            if (n == null) {
                numberCounts.put(number, 1);
            } else {
                numberCounts.put(number, ++n);
            }
        }
    }

    private void process6Combinations(final HashMap<String, Integer[]> combinations, final Integer[] numbers) {
        String key = String.format("%s%s%s%s%s%s", numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);
        combinations.putIfAbsent(key, numbers);
    }

    private void process5Combinations(final HashMap<String, Integer[]> combinations, final Integer[] numbers) {
        String key = String.format("%s%s%s%s%s", numbers[0], numbers[1], numbers[2], numbers[3], numbers[4]);
        combinations.putIfAbsent(key, numbers);
    }

    private void getRandomCombination(final HashMap<String, Integer[]> combinations) throws IOException {
        int a = 1;
        int b = 49;

        Integer random_number1 = null, random_number2 = null, random_number3 = null, random_number4 = null, random_number5 = null, random_number6 = null;

        for (int i = 1; i < 6; i++) {
            random_number1 = a + (int) (Math.random() * b);
            random_number2 = a + (int) (Math.random() * b);
            random_number3 = a + (int) (Math.random() * b);
            random_number4 = a + (int) (Math.random() * b);
            random_number5 = a + (int) (Math.random() * b);
            random_number6 = a + (int) (Math.random() * b);
        }

        Integer[] numbers = {random_number1, random_number2, random_number3, random_number4, random_number5, random_number6};
        Arrays.sort(numbers);

        String key = String.format("%s%s%s%s%s%s", numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);

        if (combinations.get(key) == null) {
            printWriter.append(String.format("%s-%s-%s-%s-%s-%s", numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]));
        }

        printWriter.append("Start checking selected combinations ... ");
        for (String combination : selectedCombinations) {
            if (combinations.get(combination) != null) {
                printWriter.append("Combination ").append(combination).append(" is in list");
            }
        }
        printWriter.append("... Finish");
    }
}
