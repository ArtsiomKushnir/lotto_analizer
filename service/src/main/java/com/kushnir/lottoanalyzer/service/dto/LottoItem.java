package com.kushnir.lottoanalyzer.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class LottoItem {
    private Integer drawSystemId;
    private String drawDate;
    private String gameType;
    private Integer multiplierValue;
    private ArrayList<LottoResult> results;
    private Boolean showSpecialResults;
    private Boolean isNewEuroJackpotDraw;
}
