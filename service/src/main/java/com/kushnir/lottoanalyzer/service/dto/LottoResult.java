package com.kushnir.lottoanalyzer.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class LottoResult {
    private String drawDate;
    private Integer drawSystemId;
    private String gameType;
    private ArrayList<Integer> resultsJson;
    private ArrayList<Integer> specialResults;
}
