package com.kushnir.lottoanalyzer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kushnir.lottoanalyzer.service.dto.LottoItem;
import lombok.RequiredArgsConstructor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.String.format;

@RequiredArgsConstructor
public class PlEuroService {

    private final String URL_LOTTO_RESULTS_JSON = "https://www.lotto.pl/api/lotteries/draw-results/by-collection-per-game?gameType=EuroJackpot&drawDate=2017-09-14&quantity=400";
    private final static String PL_EURO_LOTTO_RESULTS_FILE = "/home/akushnir/PROJECTS/lotto_analizer/console-app/src/resources/Pl_Euro_04-09-2022_results.json";
    private final static Integer[][] SELECTED_NUMBERS = new Integer[][] {{2, 16, 34, 37, 41}, {14, 17, 30, 31, 47}};
    private final static Integer[] SELECTED_SPECIAL_NUMBERS = new Integer[] {2, 5};

    private final int countToMatch;


    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Writer printWriter;

    public void handle() throws IOException {
        printWriter.append("======== EURO ========\n").append("Selected Combination: ").append(Arrays.deepToString(SELECTED_NUMBERS)).append("\n");

        String s = new String(new BufferedInputStream(new URL(URL_LOTTO_RESULTS_JSON).openStream()).readAllBytes(), StandardCharsets.UTF_8);

        final ArrayList<LottoItem> euroResults = OBJECT_MAPPER.readValue(s, OBJECT_MAPPER.getTypeFactory()
                .constructCollectionType(ArrayList.class, LottoItem.class));
        AtomicBoolean matchFound = new AtomicBoolean(false);

        euroResults.forEach(lottoItem -> {
            lottoItem.getResults().forEach(lottoResult -> {
                ArrayList<Integer> winNumbers = lottoResult.getResultsJson();
                int countMatch = 0;

                for (Integer[] givenNumber : SELECTED_NUMBERS) {
                    for (Integer n : givenNumber) {
                        if (winNumbers.contains(n)) {
                            countMatch++;
                        }
                    }

                    if(countMatch > countToMatch) {
                        matchFound.set(true);
                        try {
                            printWriter.append(format("Match -> %s,  %s%n", winNumbers, lottoResult.getDrawDate()));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    countMatch = 0;
                }
            });
        });

        if (!matchFound.get()) {
            try {
                printWriter.append("No matches found for: ").append(Arrays.deepToString(SELECTED_NUMBERS)).append("\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
