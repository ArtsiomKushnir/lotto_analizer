package com.kushnir.lottoanalyzer.service.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.time.LocalDateTime;
import java.util.Properties;

import static com.kushnir.lottoanalyzer.service.mail.MailProperties.*;

public class MailSender {
    private static final Session SESSION;
    private static final Properties PROP = new Properties() {{
        put("mail.smtp.auth", MAIL_SMTP_AUTH);
        put("mail.smtp.starttls.enable", MAIL_SMTP_STARTTLS_ENABLED);
        put("mail.smtp.host", MAIL_SMTP_HOST);
        put("mail.smtp.port", MAIL_SMTP_PORT);
        put("mail.smtp.ssl.trust", MAIL_SMTP_SSL_TRUST);
    }};

    static {
        SESSION = Session.getInstance(PROP, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MAIL_SERVER_USERNAME, MAIL_SERVER_PASSWORD);
            }
        });
    }

    public static void sendEmail(final String content) {
        try {
            Message message = new MimeMessage(SESSION);
            message.setFrom(new InternetAddress(MAIL_FROM));
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse(System.getProperty("mail.username")));
            message.setSubject("lotto report (" + LocalDateTime.now() + ")");

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(content, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
        } catch (Exception e) {
            System.out.printf("failed to send email: %s%n", e.getMessage());
        }
    }
}
