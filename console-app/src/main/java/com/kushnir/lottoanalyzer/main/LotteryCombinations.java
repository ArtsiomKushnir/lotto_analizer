package com.kushnir.lottoanalyzer.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LotteryCombinations {
    static List<Integer[]> combinations = new ArrayList<>();

    public static void generateLotteryCombinations(Integer[] combination, int start, int end, int index) {
        if (index == 5) {
            combinations.add(combination.clone());
            return;
        }
        for (int i = start; i <= end && end - i + 1 >= 5 - index; i++) {
            combination[index] = i;
            generateLotteryCombinations(combination, i + 1, end, index + 1);
        }
    }

    public static void main(String[] args) {
        int count = 0;
        Integer[] combination = new Integer[5];
        generateLotteryCombinations(combination, 1, 50, 0);
        for (Integer[] comb : combinations) {
            count++;
            System.out.println(Arrays.toString(comb));
        }
        System.out.println(count);
    }
}


// cower 100%
// 6 x 49 = 13 983 816                          42 000 000
// 5 x 50 = 2 118 760        139 838 160        1 747 977 000