package com.kushnir.lottoanalyzer.main;

import com.kushnir.lottoanalyzer.service.ByService;
import com.kushnir.lottoanalyzer.service.PlEuroService;
import com.kushnir.lottoanalyzer.service.PlService;
import org.apache.commons.math3.util.Pair;

import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

import static com.kushnir.lottoanalyzer.service.mail.MailSender.sendEmail;

public class Main {

    private final static int COUNT_ITERATIONS = 1000;
    public static void main(String[] args) throws InterruptedException {
        int countToMatch = args.length > 0 && args[0] != null ? Integer.parseInt(args[0]) : 3;
        run(countToMatch);
        // random();
    }

    static void run(int countToMatch) {
        try {
            StringWriter writer = new StringWriter();

            writer.append("============= lotto analyzer console application ============\n");

            new PlService(countToMatch, writer).handle();
            new PlEuroService(countToMatch, writer).handle();
            new ByService(writer).handle();

            writer.append("\n=============================================================\n");

            String theString = writer.toString();

            System.out.println(theString);
            sendEmail(theString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void random() {

        Map<String, Integer> data = new HashMap<>();

        Set<Integer> combination1 = generateRandom6x49(6);

        long totalAttempts = 0L, min = 9999999999999L, max=0L;
        for (int i = COUNT_ITERATIONS; i != 0; i--) {
            Set<Integer> randomResults = generateRandom6x49(6);
            int attempt = 0;
            while (true) {
                attempt++;
                if (checkMatch(randomResults, combination1)) break;
                randomResults = generateRandom6x49(6);
            }

            combination1 = generateRandom6x49(6);


            // System.out.println("Iteration[ " + i + " ] --- attempts count: " + attempt);
            totalAttempts = totalAttempts + attempt;
            max = Math.max(max, attempt);
            min = Math.min(min, attempt);

            addData(data, attempt);
        }

        System.out.println("total average: " + totalAttempts/COUNT_ITERATIONS);
        System.out.println("min: " + min);
        System.out.println("max: " + max);
        System.out.println(data);
    }

    private static void addData(Map<String, Integer> data, int attempt) {
        String keyName;
        if (attempt > 0 && attempt <= 100) {
            keyName = "1-100";
        } else if(attempt > 100 && attempt <= 1000) {
            keyName = "100-1000";
        } else if (attempt > 1000 && attempt <= 10000) {
            keyName = "1000-10000";
        } else if (attempt > 10000 && attempt <= 100000) {
            keyName = "10000-100000";
        } else if (attempt > 100000 && attempt <= 1000000) {
            keyName = "100000-1000000";
        } else if (attempt > 1000000 && attempt <= 10000000) {
            keyName = "1000000-10000000";
        } else {
            keyName = "0";
        }
        Optional.ofNullable(data.get(keyName)).ifPresentOrElse(count -> data.put(keyName, ++count), ()-> data.put(keyName, 1));
    }


    private static Set<Integer> generateRandom6x49(int system){
        // default 6
        return new Random().ints(1, 50).distinct().limit((system < 6 | system > 12) ? 6 : system)
                .boxed()
                .collect(Collectors.toSet());
    }

    @SafeVarargs
    private static boolean checkMatch(final Set<Integer> result, final Set<Integer> ... combinations) {
        for (Set<Integer> combination : combinations) {
            if (combination.containsAll(result)) return true;
        }
        return false;
    }

















    /*private static long chance(int a, int b, int system) {
        long t = b;
        long f = 1;

        if (system <= a) {
            for (long i = 1; i < a; i++) {
                f = f * (i + 1);
                t = t * (b - i);
            }
        } else {
            for (long i = 1; i < system; i++) {
                f = f * (i + 1);
            }
            for (long i = 1; i < a; i++) {
                t = t * (b - i);
            }
        }

        return t / f;
    }*/
}


// 6 0f 49, system 12, fixed numbers: average: 15005
