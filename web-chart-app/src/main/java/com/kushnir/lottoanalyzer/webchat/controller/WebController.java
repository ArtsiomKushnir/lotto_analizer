package com.kushnir.lottoanalyzer.webchat.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {

    @GetMapping("/")
    public String hello(final Model model, @RequestParam(name = "name", required = false) final String name) {
        model.addAttribute("message", name);
        return "test";
    }

}
