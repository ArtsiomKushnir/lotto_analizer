package com.kushnir.lottoanalyzer.web;

import com.kushnir.lottoanalyzer.service.ByService;
import com.kushnir.lottoanalyzer.service.PlEuroService;
import com.kushnir.lottoanalyzer.service.PlService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

@WebServlet(
        name = "lottoAnalyzer",
        urlPatterns = "/"
)
public class Controller extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding(Charset.defaultCharset().toString());

        PrintWriter printWriter = resp.getWriter();

        new PlService(printWriter).handle();
        new PlEuroService(printWriter).handle();
        new ByService(printWriter).handle();
    }
}
